/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

function printStats() {}

if (!solverSettings) var solverSettings = {
    hasFinished: function (){},                     // This is the function to check if the solver has finished solving the board
    heuristic: function(baseBoard){},               // It describes the board's situation so as to decide on a better one
    generator: function(baseBoard, pieceList){},    // This generates new states from a given state
    solveBoard: function(baseBoard, pieceList){},   // This is the main loop for solving a given board
};

// Calculates the ratio of current amount of 'full' squares out of maximum amount of 'full' squares
function generalHeuristic(baseBoard) {
    return baseBoard.reduce((t, c) => t + c.reduce((t, c) => t + (c > 0), 0), 0) / (baseBoard.length * baseBoard[0].length);
}

// Implements a depth first search to solve a board
function depthFirstSearch(baseBoard, pieceList) {
    let bestBoard = [baseBoard.map(arg => arg.slice()), solverSettings.heuristic(baseBoard)];
    let stateStack = [bestBoard[0]];
    while (!solverSettings.hasFinished(bestBoard[0]) && stateStack.length != 0) {
        let testingBoard = stateStack.pop();
        let calculatedHeuristic = solverSettings.heuristic(testingBoard);
        if (calculatedHeuristic > bestBoard[1]) { bestBoard = [testingBoard, calculatedHeuristic]; }
        let newStates = shuffle(solverSettings.generator(testingBoard, pieceList));
        for (let board in newStates) { stateStack.push(newStates[board]); }
    }
    return bestBoard[0];
}

// This implements a greedy search based on set heuristic
function greedySearch(baseBoard, pieceList) {
    let bestBoard = [baseBoard.map(arg => arg.slice()), solverSettings.heuristic(baseBoard)];
    let stateList = [bestBoard];
    while (!solverSettings.hasFinished(bestBoard[0]) && stateList.length != 0) {
        let testingBoard = stateList.shift();
        let newStates = shuffle(solverSettings.generator(testingBoard[0], pieceList));
        for (let i = 0; i < newStates.length; i++) {
            let item = [newStates[i], solverSettings.heuristic(newStates[i])];
            // Update if a better board has been found
            if (item[1] >= bestBoard[1]) {
                bestBoard = item;
                if (solverSettings.hasFinished(bestBoard[0])) { break; }
            }
            if (stateList.length == 0) { 
                stateList = [item]; 
            } else if (stateList[stateList.length - 1][1] >= item[1]) {
                stateList.push(item);
            } else {
                // A simple iteration rather than a fancy search because the array wont get toooooo big
                for (let j = 0; j < stateList.length; j++) {
                    if (item[1] >= stateList[j][1]) { 
                        stateList.splice(j, 0, item); 
                        break;
                    }
                }
            }
        }
    }
    return bestBoard[0];
}

function setGS() {
    solverSettings.generator = generateNewStates;
    solverSettings.heuristic = generalHeuristic;
    solverSettings.hasFinished = (baseBoard) => solverSettings.heuristic(baseBoard) == 1;
    solverSettings.solveBoard = greedySearch;
}

function setDFS() {
    solverSettings.generator = generateNewStates;
    solverSettings.heuristic = generalHeuristic;
    solverSettings.hasFinished = (baseBoard) => solverSettings.heuristic(baseBoard) == 1;
    solverSettings.solveBoard = depthFirstSearch;
}