//CSS Stuff
function openTab(tabName) {}
//

// This is essentially the array stuff which is presented to the screen...
let blankBoard = [
    [0,0,0,0,0],
    [0,0,0,0,0],
    [0,0,0,0,0],
    [0,0,0,0,0]
];
let displayBoard = blankBoard.map(arg => arg.slice());

// ...And the function which draws it
let drawBoard = function (){ console.error("Tried to draw the board when canvas hasn't been loaded"); };

// Pieces are defined in terms of relative position to a single point (usually the top left most square being 0,0)
let pieces = [];
let selectedPiece = 0;
// Defines the basic tetromino
pieces.push([1, [0, 0], [1, 0], [2, 0], [3, 0]]);
pieces.push([1, [0, 0], [0, 1], [0, 2], [0, 3]]);
pieces.push([2, [0, 0], [0, 1], [1, 0], [1, 1]]);
pieces.push([3, [0, 0], [0, 1], [0, 2], [1, 1]]);
pieces.push([3, [0, 0], [1, 0], [2, 0], [1, 1]]);
pieces.push([3, [0, 1], [1, 1], [2, 1], [1, 0]]);
pieces.push([3, [1, 0], [1, 1], [1, 2], [0, 1]]);
pieces.push([4, [0, 0], [1, 0], [2, 0], [2, 1]]);
pieces.push([4, [1, 0], [1, 1], [1, 2], [0, 2]]);
pieces.push([4, [0, 0], [0, 1], [1, 1], [2, 1]]);
pieces.push([4, [0, 0], [1, 0], [0, 1], [0, 2]]);
pieces.push([5, [0, 0], [0, 1], [1, 0], [2, 0]]);
pieces.push([5, [0, 0], [0, 1], [0, 2], [1, 2]]);
pieces.push([5, [0, 1], [1, 1], [2, 1], [2, 0]]);
pieces.push([5, [0, 0], [1, 0], [1, 1], [1, 2]]);
pieces.push([6, [0, 0], [1, 0], [1, 1], [2, 1]]);
pieces.push([6, [1, 0], [1, 1], [0, 1], [0, 2]]);
pieces.push([7, [0, 1], [1, 1], [1, 0], [2, 0]]);
pieces.push([7, [0, 0], [0, 1], [1, 1], [1, 2]]);

// Shown with the following functions
let drawPieces = function (){ console.error("Tried to draw pieces when canvas hasn't been loaded"); };
let setUpPieceList = function() { console.error("Tried to set up the piece list without list being loaded"); }
let showPieceSettings = function() { console.error("Tried to show piece settings without intialising the fields")}

// Essentially emulates the act of placing a piece (all pieces from 'pieceList') down on 'baseBoard'.
// Returns a list of all possible resultant states from this act
function generateNewStates(baseBoard, pieceList) {
    let returnArray = [];
    for (let i = 0; i < pieceList.length; i++) {
        let copyBoard = baseBoard.map(arg => arg.slice());
        returnArray = returnArray.concat(placePiece(pieceList[i], copyBoard, pieceList[i][0]));
    }
    return returnArray;
}
// As above but returns the result from a single piece
function placePiece(piece, baseBoard, colourVal) {
    let returnArray = [];
    for (let i = 0; i < baseBoard.length; i++ ) {
        for (let j = 0; j < baseBoard[0].length; j++) {
            let copyBoard = baseBoard.map(arg => arg.slice());
            let testBoard  = testPiecePlacement(piece, copyBoard, colourVal, [j, i]);
            if (testBoard != null) { returnArray.push(testBoard); }
        }
    }
    return returnArray;
}
// Attempts to place a piece at 'coordinate' on 'baseBoard'
// If succesful it will transform the baseBoard into one with the piece placed at 'coordinate' then return it
// If unsuccesful it will return null
function testPiecePlacement(piece, baseBoard, colourVal, coordinate) {
    for (let i = 1; i < piece.length; i++) {
        let absoluteCO = [(piece[i][0] + coordinate[0]),(piece[i][1] + coordinate[1])]; // Coordinate of point getting checked in respect to 'baseBoard'
        if ((absoluteCO[0] < 0) || (absoluteCO[0] >= baseBoard[0].length)) { return null; }
        if ((absoluteCO[1] < 0) || (absoluteCO[1] >= baseBoard.length)) { return null; }
        // Check if piece's point fits
        if (baseBoard[absoluteCO[1]][absoluteCO[0]] != 0) {
            return null
        } else {
            baseBoard[absoluteCO[1]][absoluteCO[0]] = colourVal;
        }
    }
    return baseBoard;
}

// Generates all rotations of a given piece
function rotatePiece(piece, rotations) {
    let returnList = [];
    for (let i = 0; i < rotations.length; i++)  {
        let rotatedPiece = [piece[0]];
        for (let j = 1; j < piece.length; j++) {
            let x = piece[j][0];
            let y = piece[j][1];
            let theta = rotations[i] * (Math.PI / 180);
            let newX = Math.round((x * Math.cos(theta)) - (y * Math.sin(theta)));
            let newY = Math.round((y * Math.cos(theta)) + (x * Math.sin(theta)));
            rotatedPiece.push([newX, newY]);
        }
        rotatedPiece = normalisePiece(rotatedPiece);
        returnList.push(rotatedPiece);
    }
    return returnList;
}

// Normalises the piece given to have a reference based on the top left corner
// Won't modify given piece directly instead returning the modified piece
function normalisePiece(piece) {
    let modifiedPiece = [piece[0]];
    let tempVals = piece.slice(1, piece.length);
    tempVals = tempVals.map(arg => arg.slice());
    modifiedPiece = modifiedPiece.concat(tempVals);
    let xMin = null;
    let yMin = null;
    for (let i = 1; i < modifiedPiece.length; i++) {
        let testCO = modifiedPiece[i];
        if ((xMin === null) || (testCO[0] < xMin)) { xMin = testCO[0]; }
        if ((yMin === null) || (testCO[1] < yMin)) { yMin = testCO[1]; }
    }
    xMin *= -1;
    yMin *= -1;
    if (xMin == 0 && yMin == 0) { return modifiedPiece; }
    for (let i = 1; i < modifiedPiece.length; i++) {
        let testCO = modifiedPiece[i];
        testCO[0] += xMin;
        testCO[1] += yMin;
    }
    return modifiedPiece;
}

// Define all the stuff that rquires the DOM to be loaded
document.addEventListener('DOMContentLoaded', () => {
    //CSS Stuff
    openTab = function(tabName) {
        let tabs = document.getElementsByClassName("tabs");
        for (let i = 0; i < tabs.length; i++) {
            tabs[i].style.display = 'none';
        }
        document.getElementById(tabName).style.display = 'block';
    }
    //

    // Add click event on the board which adds a 'blank' space to the board
    // Essentially a place where a piece cant be added
    document.getElementById("drawBoard").addEventListener('click', function(event) {
        let boardX = displayBoard[0].length;
        let boardY = displayBoard.length;
        let xSize = this.width / boardX;
        let ySize = this.width / boardY;
        let xVal = Math.trunc(event.layerX / xSize);
        let yVal = Math.trunc(event.layerY / ySize);
        if (blankBoard[yVal][xVal] == 0) { blankBoard[yVal][xVal] = -1; } else {  blankBoard[yVal][xVal] = 0; }
        displayBoard = blankBoard.map(arg => arg.slice());
        drawBoard();
    }, false);

    // Draws the board onto the canvas
    drawBoard = function () {
        let canvas = document.getElementById("drawBoard");
        let context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.restore();
        // Get dimensions of box
        let boxWidth = canvas.width / displayBoard[0].length;
        let boxHeight = canvas.height / displayBoard.length;
        // Draw the boxes
        for (i = 0; i < displayBoard.length; i++) {
            for ( j = 0; j < displayBoard[0].length; j++){
                let ij = displayBoard[i][j];
                // Draw Fill
                if (ij > 0) { 
                    context.fillStyle = 'rgb(' 
                                            + (255 - ((33 * ij) % 255)) + ',' 
                                            + (255 - ((63 * ij) % 255)) + ',' 
                                            + (255 - ((93 * ij) % 255)) + ')';
                    context.fillRect((j * boxWidth), (i * boxHeight), (boxWidth), (boxHeight));
                } else if (ij == -1) {
                    context.fillStyle = 'black';
                    context.fillRect((j * boxWidth), (i * boxHeight), (boxWidth), (boxHeight));
                }
                //Draw Border
                context.rect((j * boxWidth), (i * boxHeight), (boxWidth), (boxHeight));
                context.lineWidth = 2;
                context.strokeStyle = 'black';
                context.stroke();
            }
        }
    }
    drawBoard();

    drawPieces = function () {
        let canvas = document.getElementById("drawPieces");
        let context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.restore();

        let GAP = 10; // Gap between the pieces
        let baseX = 0;
        for (let i = 0 ; i < pieces.length; i++) {
            let amountSquares = pieces[i].length - 1;
            let sizeSquare = canvas.height / amountSquares;
            
            // Center the piece
            let alteredX = 0;
            let alteredY = 0;
            let mX = 0;
            let mY = 0;
            for (let j = 1; j < pieces[i].length; j++) {
                let coordinates = pieces[i][j];
                if (coordinates[0] > mX) { mX = coordinates[0]; }
                if (coordinates[1] > mY) { mY = coordinates[1]; }
            }
            alteredX = ((amountSquares * sizeSquare) - ((mX + 1) * sizeSquare)) / 2;
            alteredY = ((amountSquares * sizeSquare) - ((mY + 1) * sizeSquare)) / 2;
            // Draw piece centered
            for (let j = 1; j < pieces[i].length; j++) {
                let coordinates = pieces[i][j];
                context.fillStyle = 'rgb(' 
                                            + (255 - ((33 * pieces[i][0]) % 255)) + ',' 
                                            + (255 - ((63 * pieces[i][0]) % 255)) + ',' 
                                            + (255 - ((93 * pieces[i][0]) % 255)) + ')';
                context.fillRect((coordinates[0] * sizeSquare + baseX + alteredX), (coordinates[1] * sizeSquare + alteredY), sizeSquare, sizeSquare);
                //Draw Border
                context.rect((coordinates[0] * sizeSquare + baseX + alteredX), (coordinates[1] * sizeSquare + alteredY), sizeSquare, sizeSquare);
                context.lineWidth = 1;
                context.strokeStyle = 'black';
                context.stroke();
            }
            baseX += (amountSquares * sizeSquare + GAP);
            if (baseX >= canvas.width) { 
                canvas.width = baseX + GAP;
                drawPieces(); 
            }
        }
    }
    drawPieces();

    setUpPieceList = function() {
        let pieceList = document.getElementById("pieceList");
        for (let i = 0; i < pieces.length; i++) {
            let newItem = document.createElement('li');
            let newName = document.createTextNode("Piece " + (i + 1));
            newItem.addEventListener('click', () => { selectedPiece = i; showPieceSettings(); });
            newItem.appendChild(newName);
            pieceList.appendChild(newItem);
        }
    }
    setUpPieceList();

    showPieceSettings = function() {
        document.getElementById("pieceName").textContent = "Piece " + (selectedPiece + 1);
        let pieceCOList = document.getElementById("pieceCOList");
        while (pieceCOList.firstChild){
            pieceCOList.removeChild(pieceCOList.firstChild);
        }
        for (let i = 1; i < pieces[selectedPiece].length; i++) {
            let newItem = document.createElement('li');
            let CO = pieces[selectedPiece][i];
            newItem.appendChild(document.createTextNode(CO[0] + "," + CO[1]));
            pieceCOList.appendChild(newItem);
        }
    }
    showPieceSettings();

    document.getElementById("solveButton").addEventListener('click', () => {
        setGS();
        displayBoard = solverSettings.solveBoard(blankBoard, pieces);
        drawBoard();
    });

});


